#ifndef __PARSER_H__
#define __PARSER_H__
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>

struct Value;

struct Function
{
	void* pFunction;
	byte  argCount;
	bool  bReturn;
};

typedef std::vector<Value*> ArgList;
struct FunctionCall
{
	string name;
	ArgList arguments;
};

struct Value
{
	Value()
		: pFunctionCall(NULL)
		, pFloat(NULL)
	{}

	~Value()
	{
		if(pFloat)
			delete pFloat;
		if(pFunctionCall)
			delete pFunctionCall;
	}


	FunctionCall* pFunctionCall;
	float*		  pFloat;	
};



struct LSVariable
{
	~LSVariable()
	{
		if(value)
			delete value;
	}
	char	name;
	Value*	value;
};

enum ETokens
{
	TOKEN_DOUBLEUNDERSCORE,
	TOKEN_EQUELS,
	TOKEN_ARROW,
	TOKEN_LPARAN,
	TOKEN_RPARAN,
	TOKEN_COLON,
	TOKEN_DOUBLESLASH
};

const char* STokens[];

class CParser
{
public:
	CParser();

	bool ParseFile(const char *fileName);
	void ClearVars();

	template <typename T>
	bool RegisterFunction(const char* name, T(*function)());
	template <typename T>
	bool RegisterFunction(const char* name, T(*function)(float));
	template <typename T>
	bool RegisterFunction(const char* name, T(*function)(float, float));
	template <typename T>
	bool RegisterFunction(const char* name, T(*function)(float, float, float));

	bool CallFunction(FunctionCall* call, void* ret);
	bool CallFunction(const char* name, ArgList args, void* ret);

	const LSVariable* GetVariable(char name) const;

	bool TestVariables();

protected:
	float HandleArgument(Value* arg);
	Value* ParseValue(const std::string& str);
	bool RegisterFunction(const char* name, void* function, byte argCount, bool bReturn );

	// Parser STL functions
	void RegisterSTL();
	float static s_rand(float range);
	float static s_randr(float min, float max);

	std::ifstream* m_file;
	unsigned	 m_currentLine;
	
	// Gets line from m_file and increments m_currentLine
	void getline(string& s);

	typedef std::map<char, LSVariable*> TVariableMap;
	TVariableMap m_variables;

	typedef std::map<string, Function*> TFunctionMap;
	TFunctionMap m_functions;	
};

#endif