#include "..\util.h"
#include "..\stdAfx.h"
#include "Parser.h"

#include "..\lsystem.h"

const char* STokens[] = 
{
	"__",
	"=",
	"->",
	"(",
	")",
	":",
	"//",
};

CParser::CParser()
{
	RegisterSTL();
}

void CParser::getline(string& s)
{
	std::getline(*m_file, s);
	m_currentLine++;
}

void CParser::ClearVars()
{
	m_variables.clear();	
}


bool CParser::ParseFile(const char* filename)
{
	// Return value
	bool bSuccess = true;
	std::ifstream ifs(filename);
	if(!ifs.is_open())
	{
		ConLog("Couldn't open LSystem file: %s", filename);
		return false;
	}
	m_file = &ifs;
	m_currentLine = 1;

	char c;
	string s;
	size_t pos;
	string lastRead;	
	do{
		c =	ifs.get();
		switch(c)
		{
		case '/':
			{
				if(ifs.peek()=='/')
				{
					// Comment, skip this line
					getline(s);
				}
			}break;
		case '_':
			{
				if(ifs.peek()=='_')
				{
					// Skip the '__'
					ifs.get();
					getline(s);
					pos = s.find(':');
					if(pos == string::npos)
					{
						ConLog("ERROR: Found '__' without closing ':' \"%s\" Line: %d", m_currentLine);
						bSuccess = false;						
						break;
					}
					// Get the value between '__' and ':'
					string val = s.substr(0, pos);
					if(val=="axiom")
					{
						getline(s);
						if(!s.size() )
						{
							ConLog("ERROR: Expected axiom in line after __axiom: Line: %d", m_currentLine);
							bSuccess = false;						
							break;
						}

						s.erase(std::remove(s.begin(), s.end(), ' '));
						s.erase(std::remove(s.begin(), s.end(), '\t'));
						if(gEnv->pLSystem)
							gEnv->pLSystem->SetStart(s);
					}

				}
			}break;
		case '=':
			{
				// Get the variable that preceeded this '='
				string sVar = lastRead;				
				lastRead.clear();
				if(sVar.length()!=1)
				{
					ConLog("ERROR: Only 1 character variables supported: \"%s\" Line %d", sVar.c_str(), m_currentLine);
					bSuccess = false;
					break;
				}
				LSVariable* pVar = new LSVariable;
				string val;
				getline(val);
				pVar->value = ParseValue(val);
				pVar->name	= sVar[0];
				if(!pVar->value)
				{
					// ParseValue does error handling, so none needed here
					bSuccess = false;
					break;
				}
				m_variables.insert(TVariableMap::value_type(pVar->name, pVar));				
			}break;
		case '-':
			{
				if(ifs.peek()=='>')
				{
					string val = lastRead;
					lastRead.clear();
					if(val.length()!=1)
					{
						ConLog("ERROR: Only 1 character variables supported: \"%s\" Line %d", val.c_str(), m_currentLine);
						bSuccess = false;
						break;
					}
					// Skip the '>'
					ifs.get();
					getline(s);
					
					// Remove the whitespace
					s.erase(std::remove(s.begin(), s.end(), ' '));
					
					// Find the ':'
					size_t factorPos = s.find(':');
					float factor = 1.0f;
					if(factorPos != string::npos)
					{
						string sFactor = s.substr(factorPos+1);
						s = s.substr(0, factorPos);
						factor = atof(sFactor.c_str());
					}

					if(gEnv->pLSystem)
					{
						gEnv->pLSystem->AddProductionRule(val[0], s.c_str(), factor);
					}
				}
				else // Add it to the last read if we didn't use it
				{
					lastRead += c;
				}
			}break;
		// Skip reading white space
		case '\n':
			m_currentLine++;
		case ' ':		
		case '\t':
		case '\r':
			{

			}break;
			// When not any other case, we save the values
		default:
			{
				lastRead += c;
			}break;
		}

	}while(ifs.good());

	ifs.close();
	m_file = NULL;
	return bSuccess;
}
Value* CParser::ParseValue(const string& str)
{
	bool bSucces = true;
	Value* pValue = NULL;
	int i=0;
	// Loop until we find a character we can handle, or the string ends
	while(i<str.size())
	{
		if(str[i] >= 'A' && str[i] <='z')
		{
			// If we start out literal, check that we have parantheses, as this should be a function
			size_t lParan = str.find("(");
			size_t rParan = str.rfind(")");
			if(lParan==string::npos)
			{
				ConLog("ERROR: Couldn't find left parantheses in line %d", m_currentLine);
				bSucces = false;
			}
			if(rParan==string::npos)
			{
				ConLog("ERROR: Couldn't find right parantheses in line %d", m_currentLine);
				bSucces = false;
			}
			if(!bSucces)
				break;

			// No need to have these values cluttering up the rest, so take them out of scope
			{
				// Also make sure that we have as many '(' as ')'
				size_t lParanCount = 1;
				size_t rParanCount = 1;
				size_t pos = 0;
				for(int j=lParan+1; j<rParan; j++)
				{
					if(str[j]=='(')
						lParanCount++;
					else if(str[j]==')')
						rParanCount++;
				}			
				if(lParanCount>rParanCount)
				{
					ConLog("ERROR: Unmatched left parantheses in line %d", m_currentLine);
					bSucces = false;
				}
				else if(lParanCount<rParanCount)
				{
					ConLog("ERROR: Unmatched right parantheses in line %d", m_currentLine);
					bSucces = false;
				}
			}

			string  name = str.substr(i, lParan-i);
			TFunctionMap::const_iterator funcIt = m_functions.find(name);
			if(funcIt==m_functions.end())
			{
				ConLog("ERROR: Couldn't find function \"%s\" at line %d", name.c_str(), m_currentLine);
				bSucces = false;
				break;
			}

			if(!bSucces)
				break;
			

			pValue = new Value;
			pValue->pFunctionCall = new FunctionCall;
			pValue->pFunctionCall->name = name;
			

			/* Possible cases:

				1: f = function()
				2: f = function(123)
				3: f = function(function())
				4: f = function(function(123))
				5: f = function(123, function(123))
				6: f = function(function(123,456))
				6: f = function(function(123,456))
			*/
			// Case 1 gets no parameters, no need todo anything
			if(lParan!=rParan-1)
			{		
				// Check if the next comma is part of this value, or part of a sub value's function call
				//size_t lParan2 = str.find("(", lParan);
				//size_t comma = str.find(",", lParan);
				//pValue->pFunctionCall->arguments.push_back( ParseValue(str.substr(lParan+1, rParan-lParan-1)) );

				// Keep track of how many parantheses we have opened
				byte openParans = 0;
				char c;
				for(size_t argi=lParan+1; argi<rParan; argi++)
				{
					c = str[argi];

					if(c >= '0' && c <= '9' || c =='-') // Numerical parameter
					{
						size_t end = str.find(',', argi);
						if(end==string::npos)
						{
							pValue->pFunctionCall->arguments.push_back( ParseValue(str.substr(argi, rParan-argi)) );
							break;
						}
						else
						{
							size_t paran = str.find('(', argi);
							if(paran<end)
							{
								ConLog("Malformed function call at line %d", m_currentLine);
							}

							pValue->pFunctionCall->arguments.push_back( ParseValue(str.substr(argi, end-argi)) );
							argi = end;
						}
					}
					// Literal parameter = function
					if(c >= 'A' && c <= 'z')
					{
						// Find the starting paran and end paran 
						// (loop trough the string until we have 
						// found equal amounts of opening and closing parans

						size_t start = str.find('(', argi);
						size_t end;
						byte count = 0;
						for(size_t j=start; j<rParan; j++)
						{
							if(str[j]=='(')
								count++;
							else if(str[j]==')')
								count--;
							if(count == 0)
							{
								end = j;
								break;
							}
						}
						pValue->pFunctionCall->arguments.push_back( ParseValue(str.substr(argi, end-argi+1)) );
						argi = end;
					}
				}
			}
			
			break;
		}
		else if(str[i] >= '0' && str[i] <= '9' || str[i] =='-') // We are numerical
		{
			float* f = new float;
			*f = (float)atof(str.c_str());
			pValue = new Value;	
			pValue->pFloat = f;
			break;
		}
		else
		{
			// Found a character we can't handle, keep searching
			i++;
		}
	}

	if(!pValue)
	{
		ConLog("Couldn't read value on line %d", m_currentLine);
	}

	return pValue;
}



/*
Value* CParser::ParseValue(const string& str)
{
	bool bSucces = true;
	Value* pValue = NULL;
	int i=0;
	// Loop until we find a character we can handle, or the string ends
	while(i<str.size())
	{
		if(str[i] >= 'A' && str[i] <='z')
		{
			size_t lParan = str.find("(");
			size_t start = str.find("(");
			size_t end;
			byte count = 0;
			for(size_t j=start; j<rParan; j++)
			{
				if(str[j]=='(')
					count++;
				else if(str[j]==')')
					count--;
				if(count == 0)
				{
					end = j;
					break;
				}
			}

			string value = str.substr(lParan, end-lParan);


			size_t rParan = str.rfind(")");
			if(lParan==string::npos)
			{
				ConLog("ERROR: Couldn't find left parantheses in line %d", m_currentLine);
				bSucces = false;
			}
			if(rParan==string::npos)
			{
				ConLog("ERROR: Couldn't find right parantheses in line %d", m_currentLine);
				bSucces = false;
			}
			if(!bSucces)
				break;

			// Find the starting paran and end paran 
			// (loop trough the string until we have 
			// found equal amounts of opening and closing parans

			size_t start = str.find('(', argi);
			size_t end;
			byte count = 0;
			for(size_t j=start; j<rParan; j++)
			{
				if(str[j]=='(')
					count++;
				else if(str[j]==')')
					count--;
				if(count == 0)
				{
					end = j;
					break;
				}
			}
			pValue->pFunctionCall->arguments.push_back( ParseValue(str.substr(argi, end-argi+1)) );
			argi = end;
			}
			
			break;
		}
		else if(str[i] >= '0' && str[i] <= '9' || str[i] =='-') // We are numerical
		{
			float* f = new float;
			*f = (float)atof(str.c_str());
			pValue = new Value;	
			pValue->pFloat = f;
			break;
		}
		else
		{
			// Found a character we can't handle, keep searching
			i++;
		}
	}

	if(!pValue)
	{
		ConLog("Couldn't read value on line %d", m_currentLine);
	}

	return pValue;
}*/

const LSVariable* CParser::GetVariable(char name) const
{
	const LSVariable* pVar = NULL;
	TVariableMap::const_iterator it = m_variables.find(name);
	if(it!=m_variables.end())
	{
		pVar = it->second;
	}
	return pVar;
}

bool CParser::TestVariables()
{
	TVariableMap::const_iterator it = m_variables.begin();
	while(it!=m_variables.end())
	{
		Value* pValue = it->second->value;
		if(pValue->pFloat)
		{
			ConLog("Value \"%c\" is float: %f", it->first, *pValue->pFloat);
		}
		else if(pValue->pFunctionCall)
		{
			float f = -1.0f;
			if( CallFunction(pValue->pFunctionCall->name.c_str(), pValue->pFunctionCall->arguments, &f) )
			{
				ConLog("Variable \"%c\" is function call: %f", it->first, f);
			}
			else
			{
				ConLog("WARNING: Value \"%c\" is function call, but failed", it->first);
			}

		}
		it++;
	}

	return true;
}