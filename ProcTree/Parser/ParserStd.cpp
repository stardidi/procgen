#include "..\util.h"
#include "..\stdAfx.h"
#include "Parser.h"

void CParser::RegisterSTL()
{
	RegisterFunction("rand", &CParser::s_rand);
	RegisterFunction("randr", &CParser::s_randr);
}

float CParser::s_rand(float range)
{
	return ((float)rand()/RAND_MAX)*range;
}

float CParser::s_randr(float min, float max)
{
	return (((float)rand()/RAND_MAX)*(max-min))+min;
}
