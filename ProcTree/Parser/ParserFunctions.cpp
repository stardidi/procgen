#include "..\util.h"
#include "..\stdAfx.h"
#include "Parser.h"



template<> bool CParser::RegisterFunction<void>(const char* name, void(*function)()){
	return RegisterFunction(name, function, 0, false);
}
template<> bool CParser::RegisterFunction<float>(const char* name, float(*function)()){
	return RegisterFunction(name, function, 0, true);
}

template<> bool CParser::RegisterFunction<void>(const char* name, void(*function)(float))
{
	return RegisterFunction(name, function, 1, false);
}
template<> bool CParser::RegisterFunction<float>(const char* name, float(*function)(float)){
	return RegisterFunction(name, function, 1, true);
}

template<> bool CParser::RegisterFunction<void>(const char* name, void(*function)(float, float)){
	return RegisterFunction(name, function, 2, false);
}
template<> bool CParser::RegisterFunction<float>(const char* name, float(*function)(float, float)){
	return RegisterFunction(name, function, 2, true);
}

template<> bool CParser::RegisterFunction<void>(const char* name, void(*function)(float, float, float)){
	return RegisterFunction(name, function, 3, false);
}
template<> bool CParser::RegisterFunction<float>(const char* name, float(*function)(float, float, float)){
	return RegisterFunction(name, function, 3, true);
}


bool CParser::RegisterFunction(const char* name, void* function, byte argCount, bool bReturn)
{
	if(m_functions.find(name)!=m_functions.end())
	{
		ConLog("WARNING: Function \"%s\" already registered", name);
		return false;
	}

	Function* pFunction = new Function;
	pFunction->pFunction = function;
	pFunction->argCount = argCount;
	pFunction->bReturn  = bReturn;

	m_functions.insert(TFunctionMap::value_type(name, pFunction));
	return true;
}

float CParser::HandleArgument(Value* arg)
{
	float f = 0.0f;
	if(arg)
	{
		if(arg->pFloat)
		{
			f = *arg->pFloat;
		}
		else if(arg->pFunctionCall)
		{
			CallFunction(arg->pFunctionCall, &f);
		}
	}
	return f;
}

bool CParser::CallFunction(FunctionCall* call, void* ret)
{
	return CallFunction(call->name.c_str(), call->arguments, ret);
}

bool CParser::CallFunction(const char* name, ArgList args, void* ret)
{
	if(!name)
		return false;

	TFunctionMap::const_iterator it = m_functions.find(name);
	if(it==m_functions.end())
	{
		ConLog("ERROR: Couldn't find function \"%s\"", name);
		return false;
	}
	Function* pFunction = it->second;
	if(!pFunction)
	{
		ConLog("ERROR: Internal parser error P0001 for function \"%s\"", name);
		return false;
	}
	if(!pFunction->pFunction)
	{
		ConLog("ERROR: Internal parser error P0002 for function \"%s\"", name);
		return false;
	}
	if(pFunction->argCount!=args.size())
	{
		ConLog("ERROR: Function \"%s\" takes %d arguments, %d provided", name, pFunction->argCount, args.size());
		return false;
	}
	
	float f;
	switch(pFunction->argCount)
	{
	case 0:
		{
			if(pFunction->bReturn)	{
				f = static_cast<float(*)()>(pFunction->pFunction)();
			}else{
				static_cast<void(*)()>(pFunction->pFunction)();
			}
		}break;
	case 1:
		{
			float f0 = HandleArgument(args[0]);
			if(pFunction->bReturn) {
					f = static_cast<float(*)(float)>(pFunction->pFunction)(f0);
			}else{
				static_cast<void(*)(float)>(pFunction->pFunction)(f0);
			}
		}break;
	case 2:
		{
			float f0 = HandleArgument(args[0]);
			float f1 = HandleArgument(args[1]);
			if(pFunction->bReturn) {
					f = static_cast<float(*)(float, float)>(pFunction->pFunction)(f0, f1);
			}else{
				static_cast<void(*)(float, float)>(pFunction->pFunction)(f0, f1);
			}
		}break;
	case 3:
		{
			float f0 = HandleArgument(args[0]);
			float f1 = HandleArgument(args[1]);
			float f2 = HandleArgument(args[2]);
			if(pFunction->bReturn) {
					f = static_cast<float(*)(float, float, float)>(pFunction->pFunction)(f0, f1, f2);
			}else{
				static_cast<void(*)(float, float, float)>(pFunction->pFunction)(f0, f1, f2);
			}
		}break;
	}

	if(pFunction->bReturn && ret)
	{
		*(float*)ret = f;
	}

	return true;
}