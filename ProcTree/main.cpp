#include "stdAfx.h"
#include "tree.h"
#include "model.h"
#include "window.h"
#include "LSystem.h"
#include "util.h"

#include "Parser\Parser.h"

const char*		s_WindowTitle("ProcTree");
Vec3*			g_pLines	= NULL;
unsigned		g_lineCount = 0;
unsigned		g_glList    = 0;

char			g_sLastFile[255];
unsigned int	g_nLastRun	= 0;


DEFCMDFUNC(run)
{
	if(!s || !strlen(s))
		return;

	string result = gEnv->pLSystem->Run(atoi(s) );
	gEnv->pConsole->Log(result.c_str());
	
	g_glList = gEnv->pLSystem->RunGL();

	g_nLastRun = atoi(s);
}

DEFCMDFUNC(clear)
{
	if(gEnv->pLSystem)
	{
		gEnv->pLSystem->ClearRules();
		gEnv->pLSystem->SetStart("");
	}	
}

DEFCMDFUNC(parse)
{
	if(gEnv->pParser && gEnv->pLSystem)
	{
		gEnv->pLSystem->ClearRules();
		gEnv->pParser->ClearVars();
		gEnv->pParser->ParseFile(s);

		strcpy(g_sLastFile, s);
	}
}

DEFCMDFUNC(reload)
{
	if(gEnv->pParser && gEnv->pLSystem)
	{
		if(g_sLastFile && g_nLastRun)
		{
			gEnv->pLSystem->ClearRules();
			gEnv->pParser->ClearVars();
			gEnv->pParser->ParseFile(g_sLastFile);
			gEnv->pLSystem->Run(g_nLastRun);
			g_glList = gEnv->pLSystem->RunGL();
		}
	}
}

float function()
{
	return 2.0f;
}

float function1(float f)
{
	return f*2.0f;
}

float function2(float f, float f2)
{
	return f*f2;
}

int CALLBACK WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	bool done			= false;
	clock_t lastTime	= clock();
	CModel* pModel		= new CModel();
	WindowManager* _wnd = new WindowManager();	
	CConsole* pConsole  = NULL;
	CParser* pParser	= new CParser();
	CLSystem* pLSystem  = new CLSystem();

	if(_wnd->Init(1024,768, s_WindowTitle))
	{
		gEnv->pWindowManager = _wnd;
		pConsole = _wnd->CreateConsole();
	}

	if(pConsole)
	{
		gEnv->pConsole = pConsole;
		gEnv->pConsole->RegisterCommand("run", run);
		gEnv->pConsole->RegisterCommand("clear", clear);
		gEnv->pConsole->RegisterCommand("parse", parse);
		gEnv->pConsole->RegisterCommand("reload", reload);
	}

	gEnv->pLSystem = pLSystem;
	gEnv->pParser  = pParser;
	
	gEnv->pParser->RegisterFunction("function", &function);
	gEnv->pParser->RegisterFunction("function1", &function1);
	gEnv->pParser->RegisterFunction("function2", &function2);

	// Depends on gEnv->pParser being valid
	pLSystem->RegisterParserFunctions();

	
	while(!done)
	{
		if(_wnd->HandleMessages())
		{
			done = true;
		}else{

			_wnd->StartFrame();
			_wnd->RenderTest();
				switch(2)
				{
				case 0:					
					_wnd->RenderModel(pModel);
					_wnd->RenderModelPoints(pModel);
					break;
				case 1:
					_wnd->RenderLineList(g_pLines, g_lineCount);
					break;
				case 2:
					_wnd->RenderOGLList(g_glList);
				}
			_wnd->EndFrame();

			clock_t newTime = clock();			
			double dDiff = double(newTime - lastTime) / CLOCKS_PER_SEC * 1000;
			lastTime = newTime;
			if(dDiff < 16.667)
			{				
				Sleep(unsigned(16.667 - dDiff));
			}
		}
	}
	delete pModel;

	return 0;
}