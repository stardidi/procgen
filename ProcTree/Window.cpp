#include "Window.h"

#include <windows.h>
#include <cmath>

#include "util.h"

bool WindowManager::Init(int _width, int _height, const char* _windowName)
{
	m_nHeight		= _height;
	m_nWidth		= _width;
	m_sWindowName	= _windowName;

	if(!InitWindow())
		return false;
	if(!InitOpenGL())
		return false;

	ResetMovement();

	m_lastTime = clock();

	return true;
}

bool WindowManager::InitWindow()
{
	HINSTANCE hInst = GetModuleHandle(0);

	// Setup the windows class with default settings.
	WNDCLASSEX wc;
	wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WindowManager::s_WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInst;
	wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = "TESTWINDOW";
	wc.cbSize        = sizeof(WNDCLASSEX);

	ATOM a = RegisterClassEx(&wc) ;
	if(!a)
		return false;

	static char* title = "title";
	m_hWindow = CreateWindowA(
		"TESTWINDOW", 
	    title, 
	    WS_OVERLAPPEDWINDOW,
	    0, 
	    0, 
	    m_nWidth, 
	    m_nHeight, 
	    NULL, 
	    NULL, 
	    hInst, 
	    this);

	if (!m_hWindow)
	  return false;

	
	ShowWindow(m_hWindow, SW_SHOW);
	UpdateWindow(m_hWindow);	
	m_hDC = GetDC(m_hWindow);
	if(!m_hDC)
		return false;

	return true;
}

CConsole* WindowManager::CreateConsole()
{
	CConsole* pConsole = new CConsole();
	if(!pConsole->Init(m_hWindow) )
		return NULL;

	m_pConsole = pConsole;
	RegisterConsoleCommands();
	return pConsole;
}
void WindowManager::RegisterConsoleCommands()
{
	if(!m_pConsole)
		return;

	m_pConsole->RegisterCommand("quit", &WindowManager::quit);
}

void WindowManager::quit(const char* s)
{
	PostQuitMessage(0);
}

void WindowManager::CheckMovement(float fFrameTime)
{	
    float MoveSpeed = 2.0f;
	float LookSpeed = 1.0f;

	if(GetKeyState(VK_SHIFT) & 0x80)
	{
		MoveSpeed *= 4.0f;
		LookSpeed *= 2.0f;
	}
	if(GetKeyState(VK_CONTROL) & 0x80)
	{
		MoveSpeed *= 0.1f;
		LookSpeed *= 0.5f;		
	}

    float Distance = MoveSpeed * fFrameTime;
	float Rotation = LookSpeed * fFrameTime;

	if(GetKeyState(VK_RIGHT) & 0x80)
		m_fLookY += Rotation;
	if(GetKeyState(VK_LEFT) & 0x80)
		m_fLookY -= Rotation;
	if(GetKeyState(VK_UP) & 0x80)
		m_fLookX -= Rotation;
	if(GetKeyState(VK_DOWN) & 0x80)
		m_fLookX += Rotation;
	
	Vec3 vX = m_vX.RotateY(m_fLookY);
	Vec3 vY = m_vY.RotateAxis(vX, m_fLookX);
	m_vLook = vX.Cross(vY);

    Vec3 Up(0.0f, 1.0f, 0.0f);
    Vec3 Right = vX;
    Vec3 Forward = Right.Cross(Up);	

    Up *= Distance;
    Right *= Distance;
    Forward *= Distance;

    Vec3 Movement(ZERO);

    if(GetKeyState('W') & 0x80)
		Movement += Forward;
	if(GetKeyState('S') & 0x80)
		Movement -= Forward;
	if(GetKeyState('A') & 0x80)
		Movement -= Right;
	if(GetKeyState('D') & 0x80)
		Movement += Right;
	if(GetKeyState(VK_NEXT) & 0x80)
		Movement -= Up;
	if(GetKeyState(VK_PRIOR) & 0x80)
		Movement += Up;

	m_vEye += Movement;
}

void WindowManager::ResetMovement()
{
	m_vEye	= Vec3(ZERO);
	m_vLook = Vec3(0.0f, 0.0f, 0.0f);	
	m_fLookX = 0.0f;
	m_fLookY = 0.0f;
	m_vX = Vec3(1.0f, 0.0f, 0.0f);
    m_vY = Vec3(0.0f, 1.0f, 0.0f);
    m_vZ = Vec3(0.0f, 0.0f, 1.0f);	
}


bool WindowManager::HandleMessages() const
{
	MSG msg;
	if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if(msg.message == WM_QUIT)
		return true;
	return false;
}

LRESULT CALLBACK WindowManager::s_WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	 WindowManager *pThis; // our "this" pointer will go here
	 if (uMsg == WM_NCCREATE)
	 {
		  // Recover the "this" pointer which was passed as a parameter
		  // to CreateWindow(Ex).
		  LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
		  pThis = static_cast<WindowManager*>(lpcs->lpCreateParams);
		  // Put the value in a safe place for future use
		  SetWindowLongPtr(hwnd, GWLP_USERDATA,
						   reinterpret_cast<LONG_PTR>(pThis));
	 } else {
		  // Recover the "this" pointer from where our WM_NCCREATE handler
		  // stashed it.
		  pThis = reinterpret_cast<WindowManager*>(
					  GetWindowLongPtr(hwnd, GWLP_USERDATA));
	 }
	 if (pThis)
	 {
		  // Now that we have recovered our "this" pointer, let the
		  // member function finish the job.
		  return pThis->WndProc(hwnd, uMsg, wParam, lParam);
	 }
	 // We don't know what our "this" pointer is, so just do the default
	 // thing. Hopefully, we didn't need to customize the behavior yet.
	 return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT CALLBACK WindowManager::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		return 0;
		break;
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
	case WM_KEYDOWN:
		{
			switch(wParam)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);			
				return 0;
			case VK_HOME:
				ResetMovement();
				return 0;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
			return 0;
		}
	case WM_PAINT:
		{
			if(GetFocus() == m_hWindow)
			{
				clock_t newTime = clock();			
				float fDiff = float(newTime - m_lastTime) / CLOCKS_PER_SEC;
				m_lastTime = newTime;

				CheckMovement(fDiff);
			}
			return 0;
		}break;
	case WM_SIZE:
		{	
			m_nWidth = LOWORD(lParam);
			m_nHeight = HIWORD(lParam);
			GLResize();			

			if(gEnv->pConsole)
				gEnv->pConsole->OnResize();

			return 0;
		}break;
	case WM_MOVE:
		if(gEnv->pConsole)
				gEnv->pConsole->OnResize();
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}	
}