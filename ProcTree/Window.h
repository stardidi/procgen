#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <windows.h>
#include <ctime>
#include "util.h"
#include "Vector3D.h"

class CMesh;
class CModel;
class CConsole;

class WindowManager
{
public:
	bool Init(int width, int height, const char* windowName);	
	CConsole* CreateConsole();

	void StartFrame() const;
	void EndFrame() const;
	// Return true if quit message was posted
	bool HandleMessages() const;
	
	void RenderModel(const CModel* model);	
	void RenderModelPoints(const CModel* model);
	void RenderMesh(const CMesh* mesh);
	void RenderMeshPoints(const CMesh* mesh);
	void RenderOGLList(unsigned int i);
	// Points array should be count*(point1---->point2) == count*2 Vec3s
	void RenderLineList(const Vec3* points, unsigned count);
	void RenderCube(float x, float y, float z);
	void RenderTest();

protected:
	bool InitWindow();
	bool InitOpenGL();
	void GLResize();
	void CheckMovement(float fFrameTime);
	void ResetMovement();
	void RegisterConsoleCommands();
	DECLCMDFUNC(quit);

	int			m_nWidth;
	int			m_nHeight;
	const char*	m_sWindowName;

	HWND		m_hWindow;
	CConsole*	m_pConsole;
	HDC			m_hDC;
	HGLRC		m_hRC;
	Vec3		m_vEye;
	Vec3		m_vLook;
	Vec3		m_vX;
	Vec3		m_vY;
	Vec3		m_vZ;
	float		m_fLookX;
	float		m_fLookY;

	clock_t m_lastTime;

	LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	static LRESULT CALLBACK s_WndProc(HWND, UINT, WPARAM, LPARAM);
	
};
#endif