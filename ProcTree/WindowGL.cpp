#include "Window.h"

#include <gl/gl.h>
#include <gl/glu.h>

#include "glDefines.h"
#include "glVertices.h"

#include "model.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

bool WindowManager::InitOpenGL()
{
	int  error, iPixelFormat;

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd) );
	pfd.nSize		= sizeof(PIXELFORMATDESCRIPTOR);
	pfd.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType	= PFD_TYPE_RGBA;
	pfd.cColorBits	= 24;
	pfd.cDepthBits	= 16;
	pfd.iLayerType	= PFD_MAIN_PLANE;

	iPixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	if(!iPixelFormat)
		return false;

	error = SetPixelFormat(m_hDC, iPixelFormat, &pfd);
	if(error!=1)
		return false;

	m_hRC = wglCreateContext(m_hDC);
	if(!m_hRC)
		return false;

	error = wglMakeCurrent(m_hDC, m_hRC);
	if(error!=1)
		return false;

	error = glGetError();

	glMatrixMode(GL_PROJECTION);								// select projection matrix
    glViewport(0, 0, m_nWidth, m_nHeight);						// set the viewport
    glLoadIdentity();											// reset projection matrix
    GLfloat aspect = (GLfloat) m_nWidth / m_nHeight;
	gluPerspective(60, aspect, 0.1f, 192.0f);					// set up a perspective projection matrix
    glMatrixMode(GL_MODELVIEW);									// specify which matrix is the current matrix

	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LESS );
	glDepthMask(GL_TRUE); 
	
	glEnable (GL_LINE_SMOOTH);
    glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST );		// specify implementation-specific hints
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f, 0.2f, 0.4f, 1.0f);
	error = glGetError();
	return true;
}

void WindowManager::GLResize()
{
	glMatrixMode(GL_PROJECTION);								// select projection matrix
	glViewport(0, 0, m_nWidth, m_nHeight);						// set the viewport
	glLoadIdentity();											// reset projection matrix
	GLfloat aspect = (GLfloat) m_nWidth / m_nHeight;
	gluPerspective(60, aspect, 0.1f, 192.0f);					// set up a perspective projection matrix
	glMatrixMode(GL_MODELVIEW);									// specify which matrix is the current matrix
}
void WindowManager::StartFrame() const
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		     // Clear Screen and Depth Buffer
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(	m_vEye.x,				m_vEye.y,			 m_vEye.z,
				m_vEye.x+m_vLook.x,		m_vEye.y+m_vLook.y,	 m_vEye.z+m_vLook.z,
				0,						1,				  0);
	GLuint error = glGetError();
}

void WindowManager::EndFrame() const
{
	glFlush();
	SwapBuffers(m_hDC);
}
void WindowManager::RenderModel(const CModel* pModel)
{
	CMesh* pMesh;
	for(unsigned i=0; i<pModel->m_nMeshCount; i++)
	{
		if(pMesh = pModel->m_pMeshes[i])
		{
			RenderMesh(pMesh);
		}
	}			
}

void WindowManager::RenderModelPoints(const CModel* pModel)
{
	CMesh* pMesh;
	for(unsigned i=0; i<pModel->m_nMeshCount; i++)
	{
		if(pMesh = pModel->m_pMeshes[i])
		{
			RenderMeshPoints(pMesh);
		}
	}			
}

void WindowManager::RenderMesh(const CMesh* pMesh)
{
	if(pMesh)
	{
		glPushMatrix();
			glTranslatef(pMesh->m_vTranslation);
			glBegin(GL_TRIANGLES);			
				
				glColor3f(pMesh->m_cColor);
				for(unsigned i=0; i<pMesh->m_nTriCount; i++)
				{
					for(unsigned j=0; j<3; j++)
					{
						const Vertex& v = pMesh->m_pVertices[pMesh->m_pTriangles[i].i[j]];
						glVertex3f( v.pos);						
					}
				}
				
			glEnd();	
		glPopMatrix();
	}
}

void WindowManager::RenderMeshPoints(const CMesh* pMesh)
{
	if(pMesh)
	{
		glPushMatrix();		
			glTranslatef(pMesh->m_vTranslation.x, pMesh->m_vTranslation.y, pMesh->m_vTranslation.z);
			glColor3f(1.0f,1.0f,1.0f);
			glPointSize(3.f);
			glBegin(GL_POINTS);
				for(unsigned i=0; i<pMesh->m_nVertCount; i++)
				{
					glVertex3f( pMesh->m_pVertices[i].pos);
				}
			glEnd();		
		glPopMatrix();
	}
}

void WindowManager::RenderCube(float x, float y, float z)
{
	glPushMatrix();		
		glTranslatef(x,y,z);
		
		glColor3f(0.0f,1.0f,0.0f);
		glPointSize(4.f);
		glBegin(GL_POINTS);
			
			glVertex3f( 1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f,-1.0f, 1.0f);
			glVertex3f( 1.0f,-1.0f, 1.0f);

			glVertex3f( 1.0f, 1.0f, -1.0f);
			glVertex3f(-1.0f, 1.0f, -1.0f);
			glVertex3f(-1.0f,-1.0f, -1.0f);
			glVertex3f( 1.0f,-1.0f, -1.0f);		

		glEnd();
	glPopMatrix();
}

void WindowManager::RenderTest()
{
	glPushMatrix();
		glPointSize(4.0f);

		glBegin(GL_POINTS); //starts drawing of points
			glColor3f(1.0f,1.0f,1.0f);	// zero point
			glVertex3f(0.0f,0.0f,0.0f);

			glColor3f(0.0f,0.0f,1.0f); // Z direction
			glVertex3f(0.0f,0.0f,0.2f);
			glVertex3f(0.0f,0.0f,1.0f);
			glVertex3f(0.0f,0.0f,2.0f);
			glVertex3f(0.0f,0.0f,4.0f);
			glVertex3f(0.0f,0.0f,8.0f);
			glVertex3f(0.0f,0.0f,16.0f);

			glColor3f(0.0f,1.0f,0.0f); // Y direction
			glVertex3f(0.0f,0.2f,0.0f);
			glVertex3f(0.0f,1.0f,0.0f);
			glVertex3f(0.0f,2.0f,0.0f);
			glVertex3f(0.0f,4.0f,0.0f);
			glVertex3f(0.0f,8.0f,0.0f);
			glVertex3f(0.0f,16.0f,0.0f);

			glColor3f(1.0f,0.0f,0.0f); // X direction
			glVertex3f(0.2f,0.0f,0.0f);
			glVertex3f(1.0f,0.0f,0.0f);
			glVertex3f(2.0f,0.0f,0.0f);
			glVertex3f(4.0f,0.0f,0.0f);
			glVertex3f(8.0f,0.0f,0.0f);
			glVertex3f(16.0f,0.0f,0.0f);

		glEnd();//end drawing of points

	glPopMatrix();
	GLuint error = glGetError();
}

void WindowManager::RenderOGLList(unsigned int i)
{
	if(glIsList(i))
		glCallList(i);
}



void WindowManager::RenderLineList(const Vec3* points, unsigned count)
{
	if(!points || count==0)
		return;

	glPushMatrix();
		glLineWidth(1.5f);
		glBegin(GL_LINES);
			glColor3f(0.8f, 1.0f, 0.8f);
			For(count*2)
			{
				glVertex3f(points[i]);
			}
		glEnd();
	glPopMatrix();
}
