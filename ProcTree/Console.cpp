#include "stdAfx.h"
#include "util.h"
#include "Console.h"
#include <iostream>
#include <fstream>



bool CConsole::Init(HWND parent)
{
	m_hInst = GetModuleHandle(0);
	m_hParent = parent;	

	if(!m_hParent)
		return false;

	m_hLog = CreateConsoleSubWnd("EDIT", WS_VSCROLL|ES_READONLY|ES_AUTOVSCROLL|ES_MULTILINE|ES_LEFT, &m_pDefaultLogProc);
	if(!m_hLog)
		return false;	
	m_hInput = CreateConsoleSubWnd("EDIT", 0, &m_pDefaultInputProc);
	if(!m_hInput)
		return false;

	m_logFile.open("log.txt");
	if(!m_logFile.is_open())
		return false;
	// Remove buffering on log
	m_logFile.rdbuf()->pubsetbuf(0, 0);

	OnResize();
	RegisterCommands();
	m_pParentProc = (WNDPROC)GetWindowLongPtr(m_hParent, GWLP_WNDPROC);

	return true;
}

void CConsole::OnResize()
{
	RECT rParent;
	GetWindowRect(m_hParent, &rParent);
	int wndWidth  = rParent.right-rParent.left;
	int logHeight = 120;
	SetWindowPos(m_hLog, NULL, rParent.left, rParent.bottom, wndWidth, logHeight, SWP_SHOWWINDOW);
	SetWindowPos(m_hInput, NULL, rParent.left, rParent.bottom+logHeight, wndWidth, 20, SWP_SHOWWINDOW);
}

HWND CConsole::CreateConsoleSubWnd(const char* _class, DWORD styles/*=0*/, WNDPROC* _default/*=NULL*/)
{
	static char* title = "";
	HWND hWnd = CreateWindowA(
		_class, 
	    title, 
		WS_CHILD|WS_POPUP|styles,
		0, 0,
		100, 100,
	    m_hParent, 
	    NULL,
	    m_hInst, 
	    this);		
	
	if (!hWnd)
	{
		char c[255];
		sprintf(c, "Failed to create console subwindow of class %s: %d",_class,  GetLastError() );
		MessageBox(m_hParent, c, "", MB_ICONWARNING|MB_OK);
		return NULL;
	}

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);

	if(_default)
	{
		(*_default) = (WNDPROC)SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)&CConsole::s_ConsoleProc);		
	}
	SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)this);
	return hWnd;
}

void CConsole::Log(const char* str, ...)
{
	// Get argument list
	va_list args;
	va_start( args, str);

	// Calculate string lengths and allocate
	int wndLength = GetWindowTextLength(m_hLog);
	int strLength = vsnprintf(NULL, 0, str, args);
	char* s = new char[wndLength+strLength+3]; // 2 for \r\n and one for \0
	
	// Copy strings to new s
	GetWindowText(m_hLog, s, wndLength+1);
	vsprintf(&s[wndLength], str, args);
	va_end(args);

	// Append closing tokens
	s[wndLength+strLength]   = '\r';
	s[wndLength+strLength+1] = '\n';
	s[wndLength+strLength+2] = '\0';

	// @Memory not sure if WM_SETTEXT deallocate the old string
	SendMessage(m_hLog, WM_SETTEXT, 0, (LPARAM)s);
	SendMessage(m_hLog, EM_LINESCROLL, 0, 2<<16);

	// Log the string to the log file 
	m_logFile.write(&s[wndLength], strLength+2); // +2 to include new line
}


bool CConsole::Command(string s)
{
	string name;
	name=s.substr(0, s.find(' '));

	TCommandMap::const_iterator it = m_commandMap.find(name);
	if(it==m_commandMap.end())
	{
		Log("WARNING: Unkown command");
		return false;
	}
	
	size_t spacePos = s.find(' ');
	string args = spacePos!=string::npos ? s.substr(spacePos+1) : "";
	it->second( args.c_str());

	return true;
}

bool CConsole::RegisterCommand(const char* name, CMDFUNC function)
{
	if(!name || !strlen(name) || !function)
	{
		Log("ERROR: Registering command with invalid params");
		return false;
	}

	if(m_commandMap.size())
	{
		TCommandMap::iterator it = m_commandMap.find(name);
		if(it!=m_commandMap.end())
		{
			if(it->second == function)
				Log("ERROR: Registering already registed command with same params");
			else
				Log("ERROR: Registering already registered with same name");
			return false;
		}
	}

	m_commandMap.insert(TCommandMap::value_type(name, function));
	return true;
}

bool CConsole::UnregisterCommand(const char* name)
{
	if(!name || !strlen(name))
	{
		Log("ERROR: Unregistering command with invalid params");
		return false;
	}

	TCommandMap::const_iterator it = m_commandMap.find(name);
	if(it==m_commandMap.end())
	{
		Log("ERROR: Unregistering command which wasn't registered");
		return false;
	}

	m_commandMap.erase(it);
	return true;
}

void CConsole::RegisterCommands()
{
	RegisterCommand("exec", &CConsole::ExecuteFromFile);
}

void CConsole::ExecuteFromFile(const char* filename)
{
	std::ifstream file;
	string s;
	char c[255];
	unsigned commandCount = 0;

	if(!gEnv->pConsole)
		return;

	file.open(filename);
	if(!file.is_open())
	{
		ConLog("Couldn't read file");
		return;
	}
	
	while(file.good())
	{
		std::getline(file, s);
		if(gEnv->pConsole->Command(s) )
			commandCount++;
	}

	sprintf_s(c, 255, "Succesfully executed %d commands", commandCount);
	ConLog(c);

	file.close();
}
INT_PTR CALLBACK CConsole::s_ConsoleProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	 CConsole *pThis = reinterpret_cast<CConsole*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
	 if (pThis)
	 {
		  // Now that we have recovered our "this" pointer, let the
		  // member function finish the job.
		  return pThis->ConsoleProc(hwnd, uMsg, wParam, lParam);
	 }
	 // We don't know what our "this" pointer is, so just do the default
	 // thing. Hopefully, we didn't need to customize the behavior yet.
	 return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

INT_PTR CALLBACK CConsole::ConsoleProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Handle sending certain events from both boxes to the parent:
	switch(message)
	{
	case WM_KEYDOWN:
		switch(wParam)
		{
		case VK_ESCAPE:
			CallWindowProc(m_pParentProc, m_hParent, message, wParam, lParam);
		}
	}

	// Events for input box
	if(hWnd == m_hInput)
	{
		switch (message)
		{
		case WM_KEYDOWN:
			switch(wParam)
			{
			case VK_RETURN:
				{
					static const char* input = "Input: ";
					int length = GetWindowTextLength(m_hInput)+1+7; // 1 for \0 and 7 for "Input: "
					char* s = new char[length];
					memcpy(s, input, 7);
					GetWindowText(m_hInput, &s[7], length);
					SetWindowText(m_hInput, "");
					Log(s);
					Command(&s[7]);
				}break;
			default:
				return CallWindowProc(m_pDefaultInputProc, hWnd, message, wParam, lParam);
			}
			break;
		default:
			return CallWindowProc(m_pDefaultInputProc, hWnd, message, wParam, lParam);
		}
	}
	// Events for log box
	else if(hWnd == m_hLog)
	{
		//switch (message)
		//{
		//default:
			return CallWindowProc(m_pDefaultLogProc, hWnd, message, wParam, lParam);
		//}
	}

	// If it is somehow for a different window
	return DefWindowProc(hWnd, message, wParam, lParam);
}