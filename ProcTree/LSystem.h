#ifndef __LSYSTEM_H__
#define __LSYSTEM_H__
#include "stdAfx.h"
#include <stack>
typedef std::string string;

class CLSystem
{
public:
	CLSystem();
	~CLSystem();

	// Setup
	void RegisterParserFunctions();

	bool SetDelimiter(const string& s);
	bool SetRuleDelimiter(const string& s);
	bool AddProductionRule(const string& s, float factor=1.f);
	bool AddProductionRule(const char input, const string& output, float factor=1.f);
	bool ReadFromFile(const char* filename);

	// Sets all settings to default and wipes all rules
	void Reset();
	// Only wipes the rules
	void ClearRules();

	// Production
	string ProcessString(const string& source);
	void SetStart(const string& start);
	// Description:
	//		Applies to previously defined rules on the current string
	//		for  many times
	// Result:
	//		Returns the created string
	string Run(int n);
	// Description:
	//		Creates an opengl command list from the current string
	unsigned int RunGL();

	string GetResult();


	//bool AddConstant(string string);

protected:
	// LSystem functions registered with parser
	static void rotate(float angle);
	static void draw(float length);
	static void push();
	static void pop();
	static void color(float r, float g, float b);

	// variables for the functions above
	struct Line{
		Line() {}
		Line(Vec3 _pos, Vec3 _dir)
			: pos(_pos), dir(_dir) {}
		Vec3 pos, dir;
	};
	typedef std::stack<Line> TStack;
	TStack m_lineStack;
	Line   m_currLine;


	string m_delimiter;
	string m_ruleDelimiter;
	string m_currentString;
	struct SRule
	{
		SRule() {}
		SRule(string _s, float _f) 
			: result(_s), factor(_f)
		{}
		string result;
		float factor;
	};
	typedef std::vector<SRule> TRules;

	struct SVar
	{
		SVar() : totalFactor(0.0f) {}
		char var;
		float totalFactor;
		TRules rules;
	};

	typedef std::map<char, SVar> TVars;
	TVars m_productionRules;
	
	bool FindRule(char c, SRule& rule);
	//string m_constants[];
};

#endif