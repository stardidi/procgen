#ifndef __UTIL_H__
#define __UTIL_H__

#include "gEnv.h"
#include "Console.h"

extern GlobalEnvironment* gEnv;
#define For(max) for(unsigned i=0; i<max; i++)

#define ConLog(s, ...) if(gEnv->pConsole) gEnv->pConsole->Log(s, __VA_ARGS__);
#define DECLCMDFUNC(name) static void __stdcall name(const char* s);
#define DEFCMDFUNC(name) void __stdcall name(const char* s)


#endif