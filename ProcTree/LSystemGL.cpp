#include "LSystem.h"
#include "util.h"

#include <gl/gl.h>

#include "Parser\Parser.h"

void glVertex3f(Vec3_f vec);


void CLSystem::RegisterParserFunctions()
{
	CParser* pParser = gEnv->pParser;
	if(!pParser)
	{
		ConLog("ERROR: CLSystem::RegisterParserFunctions Parser not found, can't create GL list");
		return;
	}

	pParser->RegisterFunction("rotate", &CLSystem::rotate);
	pParser->RegisterFunction("draw", &CLSystem::draw);
	pParser->RegisterFunction("push", &CLSystem::push);
	pParser->RegisterFunction("pop", &CLSystem::pop);

	pParser->RegisterFunction("color", &CLSystem::color);
}

// LSystem draw functions
unsigned int CLSystem::RunGL()
{
	CParser* pParser = gEnv->pParser;
	if(!pParser)
	{
		ConLog("ERROR: CLSystem::RunGL Parser not found, can't create GL list");
		return 0;
	}	

	// Reset the stack and line;
	m_lineStack.empty();
	m_currLine.pos.Set(ZERO);
	m_currLine.dir.Set(0.0f, 1.0f, 0.0f);

	GLuint list = glGenLists(1);
	GLenum error = glGetError();
	glNewList(list, GL_COMPILE);
	error = glGetError();
	glBegin(GL_LINES);
	error = glGetError();
		glLineWidth(1.5f);
		glColor3f(1.0f, 0.0f, 1.0f);

		For(m_currentString.size())	
		{
			const LSVariable* pVar = pParser->GetVariable(m_currentString[i]);
			if(pVar && pVar->value && pVar->value->pFunctionCall)
			{
				pParser->CallFunction(pVar->value->pFunctionCall, NULL);
			}
		}
		error = glGetError();
	glEnd();
	error = glGetError();
	glEndList();
	error = glGetError();

	return list;
}

void CLSystem::rotate(float angle)
{
	CLSystem* pLSystem = gEnv->pLSystem;
	if(!pLSystem)
	{
		ConLog(" ERROR: CLSystem::rotate LSsytem not found");
		return;
	}

	pLSystem->m_currLine.dir = pLSystem->m_currLine.dir.RotateZ(DEG2RAD(angle));
}

void CLSystem::draw(float length)
{
	CLSystem* pLSystem = gEnv->pLSystem;
	if(!pLSystem)
	{
		ConLog(" ERROR: CLSystem::draw LSsytem not found");
		return;
	}

	glVertex3f(pLSystem->m_currLine.pos);
	pLSystem->m_currLine.pos += pLSystem->m_currLine.dir*length;
	glVertex3f(pLSystem->m_currLine.pos);
}


void CLSystem::push()
{
	CLSystem* pLSystem = gEnv->pLSystem;
	if(!pLSystem)
	{
		ConLog(" ERROR: CLSystem::push LSsytem not found");
		return;
	}

	pLSystem->m_lineStack.push(pLSystem->m_currLine);
}

void CLSystem::pop()
{
	CLSystem* pLSystem = gEnv->pLSystem;
	if(!pLSystem)
	{
		ConLog(" ERROR: CLSystem::pop LSsytem not found");
		return;
	}

	pLSystem->m_currLine = pLSystem->m_lineStack.top();
	pLSystem->m_lineStack.pop();
}


void CLSystem::color(float r, float g, float b)
{
	CLSystem* pLSystem = gEnv->pLSystem;
	if(!pLSystem)
	{
		ConLog(" ERROR: CLSystem::pop LSsytem not found");
		return;
	}

	glColor3f(r, g, b);
}
