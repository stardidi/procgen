#ifndef __GLVERTICES_H__
#define __GLVERTICES_H__
#include <gl/gl.h>
#include "Vector3D.h"


void glVertex3f(Vec3_f vec)
{
	glVertex3f(vec.x, vec.y, vec.z);
}

void glColor3f(Vec3_f vec)
{
	glColor3f(vec.x, vec.y, vec.z);
}

void glTranslatef(Vec3_f vec)
{
	glTranslatef(vec.x, vec.y, vec.z);
}

#endif //__GLVERTICES_H__