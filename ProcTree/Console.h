#ifndef __CONSOLE_H__
#define __CONSOLE_H__
#include "stdAfx.h"
#include <Windows.h>
#include <fstream>

typedef void (CALLBACK* CMDFUNC)(const char*);

class CConsole
{
public:
	// Returns true on success
	bool Init(HWND parent);
	void OnResize();
	void Log(const char* string, ...);
	bool RegisterCommand(const char* name, CMDFUNC function);
	bool UnregisterCommand(const char* name);
	bool Command(string s);
	

protected:
	// Console commands:
	void RegisterCommands();
	static void __stdcall ExecuteFromFile(const char* filename);

	HWND CreateConsoleSubWnd(const char* _class, DWORD styles=0, WNDPROC* _default=NULL);
	

	// Commands
	typedef std::map<string, CMDFUNC> TCommandMap;
	TCommandMap m_commandMap;	
	
	// Procedures
	static INT_PTR CALLBACK s_ConsoleProc(HWND, UINT, WPARAM, LPARAM);
	INT_PTR CALLBACK ConsoleProc(HWND, UINT, WPARAM, LPARAM);	
	WNDPROC m_pDefaultInputProc;
	WNDPROC m_pDefaultLogProc;
	
	// Window handles
	HWND		m_hInput;
	HWND		m_hLog;

	// Parent/process information
	HWND	m_hParent;
	WNDPROC m_pParentProc;
	HINSTANCE m_hInst;

	// Logging
	std::ofstream m_logFile;	
};

#endif __CONSOLE_H__