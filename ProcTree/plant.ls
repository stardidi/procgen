// Variables
F = draw(1)
- = rotate(25)
+ = rotate(-25)
[ = push()
] = pop()

s = rotate(-45)
c = color(0.55, 0.7, 0.2)

// Rules
X -> F-[[X]+X]+F[+FX]-X
F -> FF

__axiom:
	csX