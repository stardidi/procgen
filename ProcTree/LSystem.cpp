#include "LSystem.h"
#include "util.h"

CLSystem::CLSystem()
	: m_delimiter(";")
	, m_ruleDelimiter("->")
{	
}

CLSystem::~CLSystem()
{
}


// @Fixme This assumes the factors will be sequential [0.2;0.7;1.0] Not having so will break it!
bool CLSystem::FindRule(char c, CLSystem::SRule& _rule)
{
	// Find the var in the array
	TVars::const_iterator varIt = m_productionRules.find(c);
	if(varIt == m_productionRules.end())
		return false;


	// Calculate a value between 0..1
	float factor = (float)rand()/RAND_MAX;	

	// Loop trough the array
	const TRules& rules = varIt->second.rules;
	TRules::const_iterator ruleIt = rules.begin();
	while(ruleIt != rules.end())
	{
		if(factor <= ruleIt->factor)
		{
			_rule = *ruleIt;
			return true;
		}
		ruleIt++;
	}
	return false;
}

bool CLSystem::AddProductionRule(const string& s, float factor)
{
	size_t deliPos = s.find(m_ruleDelimiter);
	size_t endDeliPos = deliPos + m_ruleDelimiter.size();
	if(deliPos==string::npos)
		return false;

	return AddProductionRule(s.substr(deliPos-1, deliPos)[0], s.substr(endDeliPos), factor);
}

bool CLSystem::AddProductionRule(const char input, const string& output, float factor)
{
	if(!output.size())
		return false;

	TVars::iterator it = m_productionRules.find(input);
	if(it == m_productionRules.end())
	{
		it = m_productionRules.insert(TVars::value_type(input, SVar())).first;
	}	

	SVar& var = it->second;
	if(var.totalFactor + factor > 1.0f)
	{	
		ConLog("WARNING: Invalid factor for \"%c -> %s : %f\"", input, output, factor);
		return false;
	}

	
	var.rules.push_back(SRule(output, factor+var.totalFactor));
	var.totalFactor += factor;

	return true;
}

void CLSystem::Reset()
{
	ClearRules();
	m_ruleDelimiter = "->";
	m_delimiter = ";";	
}

void CLSystem::ClearRules()
{
	m_productionRules.clear();
}

string CLSystem::ProcessString(const string& source){
	string s;
	TRules::const_iterator ruleIt;
	SRule rule;
	For(source.size())
	{
		if(FindRule(source[i], rule))
		{
			s += rule.result;			
		}else{
			s += source[i];
		}
	}
	return s;
}

void CLSystem::SetStart(const string& axiom)
{
	m_currentString = axiom;
}



string CLSystem::Run(int times)
{
	For(times)
	{
		m_currentString = ProcessString(m_currentString);
	}

	return m_currentString;
}

string CLSystem::GetResult()
{
	return m_currentString;
}