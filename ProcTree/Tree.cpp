#include "stdAfx.h"
#include "tree.h"
#include "model.h"
#include "util.h"

#include <stack>

// Verts and tris need to be allocated to 8,12 respectively
void CreateCube(Vertex* pVerts, Triangle* pTris, Vec3 max, Vec3 min)
{
	// Top verts
	pVerts[0].pos = Vec3(max.x, max.y, max.z);
	pVerts[1].pos = Vec3(max.x, max.y, min.z);
	pVerts[2].pos = Vec3(min.x, max.y, min.z);
	pVerts[3].pos = Vec3(min.x, max.y, max.z);

	// Bottom verts
	pVerts[4].pos = Vec3(max.x, min.y, max.z);
	pVerts[5].pos = Vec3(max.x, min.y, min.z);
	pVerts[6].pos = Vec3(min.x, min.y, min.z);
	pVerts[7].pos = Vec3(min.x, min.y, max.z);

	memset(pTris, 0, sizeof(Triangle)*12);	
	
	for(int i=0; i<4; i++)
	{
		pTris[i*2+0].t0 = i;
		pTris[i*2+0].t1 = (i+1)%4;
		pTris[i*2+0].t2 = (i+4);

		pTris[i*2+1].t0 = (i+1)%4;
		pTris[i*2+1].t1 = ((i+1)%4)+4;
		pTris[i*2+1].t2 = (i+4);
	}
}


void Generate(CModel* pModel)
{
	static const int meshCount = 1;
	CMesh* pMesh;
	pModel->m_pMeshes = new CMesh*[meshCount];
	pModel->m_nMeshCount = meshCount;

	for(int mesh=0; mesh<meshCount; mesh++)
	{
		static const int vertCount = 8;
		static const int triCount = 12;

		pMesh = new CMesh();
		Vertex* pVerts = new Vertex[vertCount];
		Triangle* pTris = new Triangle[triCount];

		pModel->m_pMeshes[mesh] = pMesh;		
		pMesh->m_vTranslation.Set(0.0f, 0.0f, 0.0f);
		pMesh->m_cColor.Set(1.0f, 0.0f, 0.0f);
		pMesh->m_pVertices  = pVerts;
		pMesh->m_nVertCount = vertCount;		
		pMesh->m_pTriangles = pTris;
		pMesh->m_nTriCount  = triCount;
		
		/*pVerts[0].pos.Set(1.0f, 0.0f, 0.0f);
		pVerts[1].pos.Set(0.0f, 1.0f, 0.0f);
		pVerts[2].pos.Set(0.0f, 0.0f, 1.0f);

		pTris[0].t0 = 0;
		pTris[0].t1 = 1;
		pTris[0].t2 = 2;*/
		CreateCube(pVerts, pTris, Vec3(1.0f, 1.0f, 1.0f), Vec3(-1.0f, -1.0f, -1.0f) );
	}
}

/*void Generate(Vec3** _lines, unsigned& _lineCount, const string& input)
{	
	struct Line{ 
		Line(Vec3 _p1, Vec3 _p2)
			: p1(_p1), p2(_p2) {}

		Vec3 p1; Vec3 p2;
	};
	typedef Line Dir;

	typedef std::stack<Line> TStack;
	typedef std::vector<Dir> TLines;
	TStack stack;
	TLines lines;
	Dir curr(Vec3(ZERO), Vec3(0.0f, 1.0f, 0.0f));

	unsigned stringLength = input.length();
	For(stringLength)
	{
		char c = input[i];
		switch(c)
		{
		case '0':
		case '1':
		case 'F':
			{
				lines.push_back(Line(curr.p1, curr.p1+curr.p2));
				curr.p1 += curr.p2;
			}break;
		case '[':
			{
				stack.push(curr);
			}break;
		case ']':
			{
				curr = stack.top();
				stack.pop();
			}break;
		case '+':
			{
				curr.p2 = curr.p2.RotateZ(DEG2RAD(22.5));
			}break;
		case '-':
			{
				curr.p2 = curr.p2.RotateZ(DEG2RAD(-22.5));
			}break;
		}
	}

	_lineCount = lines.size();
	*_lines = new Vec3[_lineCount*2];
	For(_lineCount)
	{
		(*_lines)[i*2] = lines.at(i).p1;
		(*_lines)[i*2+1] = lines.at(i).p2;
	}
}*/

void Generate(Vec3** _lines, unsigned& _lineCount, const string& input)
{	
	struct Line{ 
		Line(Vec3 _p1, Vec3 _p2)
			: p1(_p1), p2(_p2) {}

		Vec3 p1; Vec3 p2;
	};
	typedef Line Dir;

	typedef std::stack<Line> TStack;
	typedef std::vector<Dir> TLines;
	TStack stack;
	TLines lines;
	Dir curr(Vec3(ZERO), Vec3(0.0f, 1.0f, 0.0f));

	unsigned stringLength = input.length();
	For(stringLength)
	{
		char c = input[i];
		switch(c)
		{
		case '0':
		case '1':
		case 'F':
			{
				lines.push_back(Line(curr.p1, curr.p1+curr.p2));
				curr.p1 += curr.p2;
			}break;
		case '[':
			{
				stack.push(curr);
			}break;
		case ']':
			{
				curr = stack.top();
				stack.pop();
			}break;
		case '+':
			{
				curr.p2 = curr.p2.RotateZ(DEG2RAD(22.5));
			}break;
		case '-':
			{
				curr.p2 = curr.p2.RotateZ(DEG2RAD(-22.5));
			}break;
		}
	}

	_lineCount = lines.size();
	*_lines = new Vec3[_lineCount*2];
	For(_lineCount)
	{
		(*_lines)[i*2] = lines.at(i).p1;
		(*_lines)[i*2+1] = lines.at(i).p2;
	}
}