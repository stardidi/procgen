#ifndef __MODEL_H__
#define __MODEL_H__
#pragma once

#include <string>
#include "Vector3D.h"
typedef std::string string;

struct Vertex
{
	Vec3 pos;
	Vec3 normal;
	float u, v;
};

struct Triangle
{
	Triangle() : t0(0), t1(0), t2(0) {};
	Triangle(unsigned short _t0, unsigned short _t1, unsigned short _t2) : t0(_t0), t1(_t1), t2(_t2) {};
	union
	{
		struct{
			unsigned short t0, t1, t2;
		};
		unsigned short i[3];
	};
};

class CMesh
{
public:
	CMesh() {};
	~CMesh(){ delete[] m_pVertices; delete[] m_pTriangles; };
	Vertex*		m_pVertices;
	Triangle*	m_pTriangles;

	unsigned	m_nVertCount;
	unsigned	m_nTriCount;

	Vec3		m_vTranslation;
	Vec3		m_cColor;
};

class CModel
{
public:
	CModel();
	~CModel();

	CMesh**		m_pMeshes;
	unsigned	m_nMeshCount;
};
#endif