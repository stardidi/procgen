#include "Model.h"

CModel::CModel() 
	: m_pMeshes(NULL)
	, m_nMeshCount(0)
{ 
}

CModel::~CModel()
{
	for(unsigned i=0; i<m_nMeshCount; i++)
	{
		if(m_pMeshes[i])
		{
			delete m_pMeshes[i];
			m_pMeshes[i] = NULL;
		}
	}
}