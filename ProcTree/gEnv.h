#ifndef __GENV_H__
#define __GENV_H__

class CConsole;
class CLSystem;
class WindowManager;
class CParser;

struct GlobalEnvironment
{
public:
	GlobalEnvironment() 
		: pConsole(0)
		, pLSystem(0)
		, pWindowManager(0)
		, pParser(0)
	{}

	CConsole* pConsole;
	CLSystem* pLSystem;
	WindowManager* pWindowManager;
	CParser*  pParser;

};
#endif